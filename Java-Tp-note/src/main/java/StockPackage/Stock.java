/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StockPackage;

import DatabasePackage.Database;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author prevostn
 */
public class Stock {
    
   ArrayList<Jeu> listJeu = new ArrayList();

    public Stock() 
    {
        loadAllGames();
    }
    
    /*
     * Charge les jeux contenus dans la DB
     */
    private void loadAllGames() {
        ResultSet rs = Database.getInstance().getResultOf("SELECT id, title, price, players, in_stock FROM BoardGame;");
       try {
           while(rs.next()){
               listJeu.add(new Jeu(
                       rs.getInt("id"),
                       rs.getString("title"),
                       rs.getDouble("price"),
                       rs.getString("players"),
                       rs.getInt("in_stock")
               ));
           }
       } catch (SQLException ex) {
           Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    /**
     * Ajoute un jeu dans la liste et dans la DB.
     * @param game
     * @return 
     */
    public boolean addGame(Jeu game) {
        Database db = Database.getInstance();
        String query = "INSERT INTO BoardGame(title, price, players, in_stock) VALUES((?), (?), (?), (?));";
        Long id = 0L;
       try {
           PreparedStatement preparedStatement = db.getConnection().prepareStatement(query);
           preparedStatement.setString(1, game.getTitre());
           preparedStatement.setDouble(2, game.getPrix());
           preparedStatement.setString(3, game.getJoueurs());
           preparedStatement.setInt(4, game.getQuantite());
           
           preparedStatement.executeUpdate();
           id = db.getLastInsertedId(preparedStatement);
       } catch (SQLException ex) {
           Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
           return false;
       }
       game.setId(id.intValue());
       listJeu.add(game);
       return true;
    }
    
    /**
     * Supprime un jeu de la liste et de la DB
     * @param id
     * @return 
     */
    public boolean deleteGame(int id)
    {
        Database db = Database.getInstance();
        try 
        {
           String query = "DELETE FROM BoardGame WHERE id = (?)";
           PreparedStatement preparedStatement = db.getConnection().prepareStatement(query);
           preparedStatement.setInt(1, id);
           preparedStatement.executeUpdate();

        } catch (SQLException ex) 
        {
           Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
           return false;
        }
        
        return true;
    }
    
    /**
     * Met à jours un jeu en DB
     * @param game
     * @return 
     */
    public boolean updateGame(Jeu game) {
        Database db = Database.getInstance();
        try 
        {
           String query = 
                     "UPDATE BoardGame "
                   + "SET title = (?), price = (?), players = (?), in_stock = (?) "
                   + "WHERE id = (?);";
           PreparedStatement preparedStatement = db.getConnection().prepareStatement(query);
           preparedStatement.setString(1, game.getTitre());
           preparedStatement.setDouble(2, game.getPrix());
           preparedStatement.setString(3, game.getJoueurs());
           preparedStatement.setInt(4, game.getQuantite());
           preparedStatement.setInt(5, game.getId());
           preparedStatement.executeUpdate();

        } catch (SQLException ex) 
        {
           Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
           return false;
        }
        
        return true;
    }
    
    /**
     * Enlève un jeu de la liste à partir de son id
     * @param id 
     */
    public void removeGameFromList(int id) {
        for(int i = 0; i < listJeu.size(); i++) {
            if(listJeu.get(i).getId() == id) {
                listJeu.remove(i);
            }
        }
    }
    
    /**
     * Renvoie un jeu à partir de son ID passé en paramètre
     * @param id
     * @return 
     */
    public Jeu getGameById(int id) {
        for(Jeu game : listJeu)
            if(game.getId() == id)
                return game;
        return null;
    }
  

    public ArrayList<Jeu> getListJeu() {
        return listJeu;
    }

    public void setListJeu(ArrayList<Jeu> listJeu) {
        this.listJeu = listJeu;
    }
   
   
   
    
    
}
