package StockPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author prevostn
 */
public class Jeu {
   
    private String titre, joueurs;
    private double prix;
    private int quantite, id;

      public Jeu(int id, String titre, double price, String joueurs, int quantite) {
        this.id = id;
        this.titre = titre;
        this.prix = price;
        this.joueurs = joueurs;
        this.quantite = quantite;
    }

    
    public Jeu(String titre, double price, String joueurs, int quantite) {
        this.titre = titre;
        this.prix = price;
        this.joueurs = joueurs;
        this.quantite = quantite;
    }

    
    
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(String joueurs) {
        this.joueurs = joueurs;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
}
