/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FramePackage;

import StockPackage.Jeu;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Theo
 */
public class JTableListener implements ListSelectionListener {

    private boolean mouseReleaseEvent;
    private PanelJTable panel;
    
    public JTableListener(PanelJTable panel) {
       this.panel = panel;
    }
    
    /**
     * Est appelée lorsque l'utilisateur presse ou relache la souris sur une ligne 
     * de la JTable, d'ou le booleen mouseReleaseEvent qui évite que la fonction soit
     * appelée deux fois sur la même case.
     * Ensuite va chercher le jeu à partir de son ID et appele la fonction "fillField"
     * qui remplira les champs de saisi avec les attribut de l'objet.
     * @param e 
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        mouseReleaseEvent = !mouseReleaseEvent;
        if(mouseReleaseEvent) {
            JTable table = panel.getTable();
            int row = table.getSelectedRow();
            if(row == -1)
                return;
            int gameId = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
            Jeu game = panel.getStock().getGameById(gameId);

            if(game != null) {
                panel.fillField(game);
                panel.editGameMode();
            }
        }
        
    }
    
}
