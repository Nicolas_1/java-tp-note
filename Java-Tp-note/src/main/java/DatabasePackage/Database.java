package DatabasePackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {

    private String dbPath;
    private static Connection connection;
    private Statement statement;
 
    private static volatile Database instance = null;
    private Database() {
        super();
        
    }
    public final static Database getInstance() {
        if (null == instance) { 
            instance = new  Database();
        }
        return instance;
    }
    
    /**
     * Connectuin à la DB
     */
    public void connect() {
        String sqlite3path =  String.format("jdbc:sqlite:%s", dbPath);
        try {
            Class.forName("org.sqlite.JDBC");
            Database.connection = DriverManager.getConnection(sqlite3path);
            statement = connection.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Ferme la connection à la base de données
     */
     public void closeConnection() {
        try {
             if (Database.connection != null) 
                Database.connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Permet de faire une requête SQL
     *
     * @param requete La requête SQL (avec un ";" à la fin)
     * @return Un ResultSet contenant le résultat de la requête
     */
    public ResultSet getResultOf(String requete) {
        ResultSet rs = null;
        try {
            rs = this.statement.executeQuery(requete);
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    /**
     * Retourne l'id du dernier élément inséré par la requète préparée passée en
     * paramètre
     * 
     * @param st
     * @return id du dernier élément inséré en base
     * @throws SQLException 
     */
    public long getLastInsertedId(PreparedStatement st) throws SQLException {
        long id = -1;
        try (ResultSet generatedKeys = st.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        }
        return id;
    }

    // Getter et setter
    public String getDbPath() {
        return dbPath;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }
    
    public void setDbPath(String path) {
        this.dbPath = path;
    }
}
