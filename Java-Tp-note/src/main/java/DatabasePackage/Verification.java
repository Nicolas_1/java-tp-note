/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabasePackage;

import java.io.File;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author prevostn
 */
public class Verification 
{
    /**
     * Vérifie que l'extension du fichier en paramètre est bien une DB sqlite3 et
     * que celle ci existe bien.
     * @param path
     * @return 
     */
    static public boolean dbCheckPath(String path)
    {
        
        String[] tab = path.split("\\.");
        File base = new File(path);
        if(tab.length == 0 || !tab[tab.length-1].equals("sqlite3") )   // chemin vide ou mauvaise extension
        {
            JOptionPane optionPane = new JOptionPane("Extension incorrect", JOptionPane.ERROR_MESSAGE);
            JDialog dialog = optionPane.createDialog("Extension incorrecte : .sqlite3 attendu");
            dialog.setVisible(true);
            return false;
        }
        else if(!base.exists())
        {
            JOptionPane optionPane = new JOptionPane("Fichier introuvable", JOptionPane.ERROR_MESSAGE);
            JDialog dialog = optionPane.createDialog("Fichier introuvable : base de données attendue");
            dialog.setVisible(true);
            return false;
        }
        
        return true;
    }
     
    
    
}
