DROP TABLE IF EXISTS "BoardGame";
PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS "BoardGame" (
  'id' INTEGER  NOT NULL PRIMARY  KEY AUTOINCREMENT UNIQUE,
  'title' VARCHAR(30) NOT NULL,
  'price' DOUBLE NOT NULL,
  'players' VARCHAR(3) NOT NULL,
  'in_stock'  INTEGER NOT NULL
);

INSERT INTO BoardGame(title, price, players, in_stock) VALUES ('Risk', 100.99, '4', 10);
INSERT INTO BoardGame(title, price, players, in_stock) VALUES ('Monopoly', 49.99, '10', 5);