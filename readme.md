# BoardGameManager

**Made by Théo & Nico**


[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

----------------

Ce projet consiste en un logiciel permettant la gestion d'un stock de jeux de plateau.
Ce gestionnaire sera synchronisé avec une base de données que l'utilisateur choisira.
L'utilisateur pourra créer des jeux dans le gestionnaire, mais également modifier ou supprimer ceux déjà présents.

----------------

## diagramme UML des classes

![diagramme UML](Java-Tp-note/res/uml/Classdiagram1.png)


## Etat du projet
**Projet Terminé**  :heavy_check_mark:

##### Améliorations futures possibles :

* Etoffer la base de données pour avoir des fournisseurs
* Pouvoir ajouter/supprimer/modifier des fournisseurs

## License

&copy; Open Source :+1:

## Installation

* **Sous Unix / Windows :**

1 ) Télécharger un IDE Java tel que NetBeans (au hasard...)

2 ) Vous aurez besoin de Maven (gestionnaire de dépendances) pour faire fonctionner le projet ( Maven est déjà inclu dans NetBeans)

3 ) Importer le projet Java dans l'IDE

## Démarrage

* **Sous Unix / Windows :**

1 ) Lancer l exécution du programme depuis l'IDE

## Développé avec :

* Java
* SQLite3

## Librairies Java :

	* SQLite-jdbc
